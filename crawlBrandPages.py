import pandas as pd 
import json
import requests as rq
import urllib3 as urllib
import csv
from endpoints.ml.Scraper import *
import pickle as pkl
import os
from endpoints.ml.ml import *

def getBrands():
	Brands = pd.read_excel("data/Brands_Dataset.xlsx")
	return Brands

def getBrandData():
	if os.path.exists("branddata.csv"):
		Brands = pd.read_csv("branddata.csv")
		return Brands
	else:
		return None

def doRequest(brandName):
	apiRoot = "https://autocomplete.clearbit.com/v1/companies/suggest?query="
	url = apiRoot + brandName
	try:
		response = rq.get(url)
		if response.status_code == 200:
			print response.content
			# json.dump(response.json(),open(fileName,'w'))
			return json.loads(response.content)
		else:
			[]
	except:
		print "nothing worked"
		return []




if __name__ == '__main__':
	scraper = Scraper()
	brands = getBrands()
	bdata = getBrandData()
	crawled = []
	# if bdata!=None:
	# 	for col, row in bdata.iterrows():
	# 		bdata.append(row['Name'])
	# for row
	fieldnames = ['Name', 'url' , 'description']
	with open("branddata2.csv", 'a') as output_file:
		dict_writer = csv.DictWriter(output_file, fieldnames=fieldnames)
		for col, row in brands.iterrows():
			brandName = row['Brand name']
			if brandName not in crawled:
				try:
					response = doRequest(brandName)
				except:
					print " The API barfed!! "
					continue
				if len(response) > 0:
					domain =  response[0]['domain']
					url = "http://www." + domain
					try:
						text = scraper.scrape(url).encode("utf-8")
						print text
						dict_writer.writerow({'Name': brandName , 'url': url , 'description': text})
					except:
						continue
		

			


import os, json, pandas

rootdir = os.path.expanduser('~') + '/hm_futureofads_platform'
extension = '.json'


# 2304 Personas:

data = []
for subdir, dirs, files in os.walk(rootdir + '/data/GWIData'):
    for file in files:
        ext = os.path.splitext(file)[-1].lower()
        if ext == extension:
            persona_questions = json.load(open(os.path.join(subdir, file)))['data']
            split_name = file.split('.')[0].split('_')
            social_media = split_name[-1][:-len('gwi-ext')]  # removes question prefix from filename
            for option in persona_questions:
                data += [split_name[:-1] + [social_media] + [option['option']] + [option['count']]]

data = pandas.DataFrame(data, columns=
                        ['Demographics', 'Marital Status', 'Education',
                         'Social Segmentation', 'Personal Interests',
                         'Attitude', 'Social Networks', 'answer', 'count'])

questions = {'gwi-ext.q1030a': 'luxury',
             'gwi-ext.q1033a': 'drinks',
             'gwi-ext.q1038': 'haircare-skincare-cosmetics'}

questions_info = json.load(open(os.path.join(rootdir, 'questions.json')))['data']

relevant_questions = [qn for qn in questions_info if qn['id'] in questions.keys()]
option_names = {}
for qn in relevant_questions:
    for opt in qn['available_options']:
        option_names[opt['code']] = opt['name']

data['answer'] = data['answer'].replace(option_names)
data.to_pickle(rootdir + '/personas_and_brands.pickle')


# 22 Audiences:

marg = []
fields = set()
for subdir, dirs, files in os.walk(rootdir + '/data/GWIData'):
    for file in files:
        ext = os.path.splitext(file)[-1].lower()
        if ext == extension:
            persona_questions = json.load(open(os.path.join(subdir, file)))['data']
            for option in persona_questions:
                option_name = option['option']
                for aud in option['by_audience']:
                    fields = fields.union(set(aud.keys()))
                    marg += [[option_name,
                              aud['audience'],
                              aud['responses_count'],
                              aud['count'],
                              aud['horizontal_percentage'],
                              aud['index'],
                              aud['percentage'],
                              aud['weighted_universe_count'],
                             ]]

marg = pandas.DataFrame(marg, columns=['answer', 'audience', 'responses_count',
                                       'count', 'horizontal_percentage', 'index',
                                       'percentage', 'weighted_universe_count']).drop_duplicates()

marg['answer'] = marg['answer'].replace(option_names)

audiences_info = json.load(open(os.path.join(rootdir, 'personaAudiences.json')))
audience_names = {}
for aud in audiences_info.values():
    for key in aud.keys():
        audience_names[key] = aud[key]

marg['audience'] = marg['audience'].astype(str).replace(audience_names)
marg.to_pickle(rootdir + '/audiences_and_brands.pickle')

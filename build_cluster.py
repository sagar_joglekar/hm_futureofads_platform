#!/usr/bin/python
import os
import sys
# from endpoints import backend
# from storage import data_models

# from flask import Flask, render_template, request
# import json
# from flask_cors import CORS
# import datetime
# # from endpoints.backend import *
# from endpoints.ml_backend import *
import pandas
import ctypes
from os import listdir
from os.path import isfile, join
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.stem import WordNetLemmatizer
from math import log, sqrt
from random import uniform
from copy import deepcopy
import numpy as np
from scipy.spatial import distance

stopwords = set(stopwords.words('english'))

word_dict = {}
company_dict = {}
tf_idf_dict = {}
cluster_matrix = []
cluster = []

wnl = WordNetLemmatizer()

def extract_comp():
	in_file = pandas.read_csv('us_companies.csv')
	keep_col = ['company_name', 'url', 'company_category', 'description', 'description_short']
	out_file = in_file[keep_col]
	out_file.to_csv('comp.csv', index=False)

def preprocess(file):
	companies = pandas.read_csv(file)['company_name']
	descriptions = pandas.read_csv(file)['description']
	descr_dict = dict(zip(companies,descriptions))

	for company, word_str in descr_dict.iteritems():
		company_dict[company] = {}
		tf_idf_dict[company] = {}

		word_list = word_tokenize(unicode(word_str, errors = "ignore").lower())
		word_set = set()

		for word in word_list:
			if len(word) < 3:
				continue

			if word in stopwords:
				continue

			lemm_word = wnl.lemmatize(word)

			if lemm_word not in word_set:
				word_set.add(lemm_word)

				if word_dict.get(lemm_word) is None:
					word_dict[lemm_word] = 1
				else:
					word_dict[lemm_word] += 1

			if company_dict[company].get(lemm_word) is None:
				company_dict[company][lemm_word] = 1
			else:
				company_dict[company][lemm_word] += 1

	for word in word_dict.keys():
		if word_dict[word] < 15:
			del word_dict[word]

	print "Words no: ", len(word_dict.keys())

def tf_idf():
	nr_doc = len(company_dict.keys())
	for file in company_dict.keys():
		for word in word_dict.keys():
			if company_dict[file].get(word) is None:
				tf_idf_dict[file][word] = 0
			else:
				TF = company_dict[file][word]
				IDF = log((nr_doc - word_dict[word] + 0.5) / (word_dict[word] + 0.5))
				tf_idf_dict[file][word] = TF * IDF

def clustering():
	nr_files = len(tf_idf_dict.keys())
	tf_idf_matrix = np.zeros( (len(company_dict.keys()), len(word_dict.keys())) )
	cluster_matrix = np.zeros( (nr_files, nr_files) )
	global cluster

	i = 0
	for file in tf_idf_dict.keys():
		j = 0
		cluster.append([file])
		for word in tf_idf_dict[file].keys():
			tf_idf_matrix[i][j] = tf_idf_dict[file][word]
			j += 1
		i += 1
	print "Matrix copy Done ---------------"

	for i in range(len(tf_idf_matrix)):
		for j in range(i, len(tf_idf_matrix)): 
			cluster_matrix[i][j] = distance.euclidean(tf_idf_matrix[i], tf_idf_matrix[j])
			cluster_matrix[j][i] = cluster_matrix[i][j]

	for i in range(len(cluster_matrix)):
		cluster_matrix[i][i] = np.inf
	print "Distance matrix Done -----------"

	while len(cluster_matrix) > 8:
		(i, j) = np.unravel_index(cluster_matrix.argmin(), cluster_matrix.shape)

		for k in range(len(cluster_matrix)):
			if i == k:
				continue
			if j == k:
				continue

			num = float(len(cluster[i]) + len(cluster[j]) + len(cluster[k]))
			alpha_i = float(len(cluster[i]) + len(cluster[k])) / num
			alpha_j = float(len(cluster[j]) + len(cluster[k])) / num
			beta = -float(len(cluster[k]) / num)

			cluster_matrix[i][k] = float(alpha_i * cluster_matrix[k][i]) + float(alpha_j * cluster_matrix[k][j]) + float(beta * cluster_matrix[i][j])
			cluster_matrix[k][i] = cluster_matrix[i][k]

		cluster_matrix = np.delete(cluster_matrix, j, 0)
		cluster_matrix = np.delete(cluster_matrix, j, 1)

		cluster[i].extend(cluster[j])
		cluster = np.delete(cluster, j, 0)
		print len(cluster_matrix)

	print "Clustering DONE ----------------"
	print "Start Clasification --------------"
	# match(brand, cluster)

def get_cluster(name, description):
	preprocess('comp.csv')
	tf_idf()
	clustering()
	for i in range(len(cluster)):
		if name in cluster[i]:
			print i 

def main():
	# extract_comp()
	get_cluster('Lucid', 'a')

if __name__ == "__main__":
	main();

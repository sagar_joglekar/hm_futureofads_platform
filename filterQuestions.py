import json
import requests as rq 
import os
from itertools import product
import multiprocessing as mp
from random import shuffle

headers = {
	"Content-Type" : "application/json",
	"Authentication" : "Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzAwOTQxMzEsImV4cCI6MTUzMDE4MDUzMSwicm9sIjpbInVzZXIiXSwiaWQiOjU2NTA2LCJwZW0iOlsxMCw3LDFdfQ.SKWiRzCmAa437aOxMVSSJkN91ZLSIU8egV13_SJrwnM"
}

apiRoot = "https://api.globalwebindex.net/q"

dataRoot = "data/GWIdata/"

questionKeys = json.load(open("QuestionsKeys.json",'rb'))
personaAudiences = {
    "demographics":
    {
        "5000": "Male 16-24",
        "5001": "Male 25-34",
        "4994": "Female 16-24",
        "4995": "Female 25-34",
    },

    "Maritial Status":
    {
        "5014": "married",
        "5012": "single"
    },

    "Education":
    {
        "5017": "High School",
        "5019": "University"
    },

    "Social Segmentation":
    {
        "32518": "Brand Followers",
        "172997": "Shoppers",
        "106874": "Sharers"
    },

    "Personal Interests":
    {
        "172994": "Art and culture",
        "5032": "Health , fitness and Beauty",
        "5030": "Tech Enthusiasts",
        "32514": "Fashion",
    },

    "Attitude":
    {
        "198381": "Alturistic",
        "198385": "Cosmopolitian",
        "198386": "Economical",
        "198390": "Spontaneous"
    },
    
    "Social Networks":
    {
        "32404": "Facebook",
        "32363": "Instagram",
        "32361": "Twitter"
    }

}


personaRootTriats = ["demographics" , "Maritial Status" , "Education" , "Social Segmentation" , "Personal Interests" ,"Attitude" , "Social Networks"]

def doRequest(payload,personaKeys,question, files):
	print len(files)
	name = '_'.join(personaKeys) + question + ".json"
	fileName = dataRoot + name
	print fileName
	if name in files: 
		print "already done"
		return 404
	else: 
		response = rq.post(apiRoot, headers = headers , data = json.dumps(payload))
		print response.status_code
		json.dump(response.json(),open(fileName,'w'))
		return response.status_code


def getTupleList(dictionary):
	return [(k,dictionary[k]) for k in dictionary]

def getPersonaProduct(personaAudiences):
	listofAttrs = [getTupleList(personaAudiences[k]) for k in personaRootTriats]
	listofTuples = []
	for k in product(*listofAttrs):
		listofTuples.append(k)
	return listofTuples


if __name__ == '__main__':
	pool = mp.Pool(processes=6) 
	personaTouples = getPersonaProduct(personaAudiences)
	shuffle(personaTouples)
	print "Found %d persona combinations in total!! " %(len(personaTouples))
	files = os.listdir(dataRoot)
	for listt in personaTouples:
		keys = [k[1] for k in listt]
		data = {}
		data['audiences'] = [k[0] for k in listt]
		for q in range(len(questionKeys)):
			data['question'] = questionKeys[q][1]
			print "Doing request for : " + '_'.join(keys)
			f = pool.apply_async(doRequest, (data, keys , questionKeys[q][1] , files))
			num_front= f.get() 
			print num_front
	pool.close()
	pool.join()






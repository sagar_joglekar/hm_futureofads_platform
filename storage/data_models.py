from mongoengine import *

def connect_to_mongo():
    print('I am connecting to MongoDB ')
    connect('mongoengineMarchTech', host='192.168.15.124', port=27017)

import json
from bson import ObjectId

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)
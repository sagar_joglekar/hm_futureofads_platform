from backend import app
from ml.Scraper import *
from ml.ml import *
from flask import Flask, render_template, request
from cluster import Cluster

@app.route("/keywordText",methods=['POST', 'GET'])
def keyword():
    inbound_message = request.args.get('text')
    return extractKeywordsText(inbound_message)

@app.route("/scrape", methods=['POST', 'GET'])
def scrape():
    """Respond to incoming calls with a simple text message."""
    print request.form
    url = request.args.get('url')
    print "Scraping : " + url
    scraper =  Scraper()
    text = scraper.scrape(url)
    return ' '.join(text.split())

@app.route("/summarize", methods=['POST', 'GET'])
def summ():
    """Respond to incoming calls with a simple text message."""
    print request.form
    inbound_message = request.args.get('text')
    print "recieved : " + inbound_message
    return summarizeText(inbound_message)

@app.route("/logo", methods=['POST', 'GET'])
def logoExtractor():
    inbound_message = request.args.get('url')
    scraper =  Scraper()
    return scraper.extractLogo(inbound_message)  


def perform_match(keywords,antiKeywords=None):
    result = None
    return result

@app.route("/match", methods=['POST', 'GET'])
def match():
    keywords=request.args.get('keywords').split(' ')
    result=perform_match(keywords)
    import json
    return json.dumps(result)

@app.route("/cluster", methods=['POST','GET'])
def cluster():
    name = request.args.get('name').encode("utf-8")
    description = request.args.get('description').encode("utf-8")
    category = request.args.get('category').encode("utf-8")
    gender = request.args.get('gender').encode("utf-8")
    age = request.args.get('age').encode("utf-8")
    personal_intrest = request.args.get('pi').encode("utf-8")
    attitude = request.args.get('attitude').encode("utf-8")
    pers = (gender, age, personal_intrest, attitude)

    clus = Cluster(name, description, pers)
    return json.dumps(clus.get_cluster())

@app.route("/prediction", methods=['POST','GET'])
def prediction():
    name = request.args.get('name').encode("utf-8")
    description = request.args.get('description').encode("utf-8")
    category = request.args.get('category').encode("utf-8")
    gender = request.args.get('gender').encode("utf-8")
    age = request.args.get('age').encode("utf-8")
    personal_intrest = request.args.get('pi').encode("utf-8")
    attitude = request.args.get('attitude').encode("utf-8")
    pers = (gender, age, personal_intrest, attitude)

    clus = Cluster(name, description, pers)
    return json.dumps(clus.get_prediction())
#!/usr/bin/python
import os
import sys
# from endpoints import backend
# from storage import data_models

# from flask import Flask, render_template, request
# import json
# from flask_cors import CORS
# import datetime
# # from endpoints.backend import *
# from endpoints.ml_backend import *
import pandas
import ctypes
import json
from os import listdir
from os.path import isfile, join
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.stem import WordNetLemmatizer
from math import log, sqrt
from random import uniform, SystemRandom
from copy import deepcopy
import numpy as np
from scipy.spatial import distance

class Cluster:
	def __init__(self, name, description, pers):
		self.name = name
		self.description = description
		self.word_dict = {}
		self.company_dict = {}
		self.tf_idf_dict = {}
		self.cluster_matrix = []
		self.cluster = []
		self.category = ["Tech", "Bussiness", "Environment", "Media", "Lifestyle", "Education", "Food&Health"]
		self.pers = pers

	def preprocess(self, file):
		companies = pandas.read_csv(file)['company_name']
		descriptions = pandas.read_csv(file)['description']
		descr_dict = dict(zip(companies,descriptions))
		descr_dict[self.name] = self.description
		stopword = set(stopwords.words('english'))
		wnl = WordNetLemmatizer()

		for company, word_str in descr_dict.iteritems():
			self.company_dict[company] = {}
			self.tf_idf_dict[company] = {}

			word_list = word_tokenize(unicode(word_str, errors = "ignore").lower())
			word_set = set()

			for word in word_list:

				if word in stopword:
					continue

				lemm_word = wnl.lemmatize(word)

				if lemm_word not in word_set:
					word_set.add(lemm_word)

					if self.word_dict.get(lemm_word) is None:
						self.word_dict[lemm_word] = 1
					else:
						self.word_dict[lemm_word] += 1

				if self.company_dict[company].get(lemm_word) is None:
					self.company_dict[company][lemm_word] = 1
				else:
					self.company_dict[company][lemm_word] += 1

		for word in self.word_dict.keys():
			if self.word_dict[word] < 25:
				del self.word_dict[word]


		print "Words no: ", len(self.word_dict.keys())

	def tf_idf(self):
		nr_doc = len(self.company_dict.keys())
		for file in self.company_dict.keys():
			for word in self.word_dict.keys():
				if self.company_dict[file].get(word) is None:
					self.tf_idf_dict[file][word] = 0
				else:
					TF = self.company_dict[file][word]
					IDF = log((nr_doc - self.word_dict[word] + 0.5) / (self.word_dict[word] + 0.5))
					self.tf_idf_dict[file][word] = TF * IDF

	def clustering(self):
		nr_files = len(self.tf_idf_dict.keys())
		tf_idf_matrix = np.zeros( (len(self.company_dict.keys()), len(self.word_dict.keys())) )
		self.cluster_matrix = np.zeros( (nr_files, nr_files) )

		i = 0
		for file in self.tf_idf_dict.keys():
			j = 0
			self.cluster.append([file])
			for word in self.tf_idf_dict[file].keys():
				tf_idf_matrix[i][j] = self.tf_idf_dict[file][word]
				j += 1
			i += 1
		print "Matrix copy Done ---------------"

		for i in range(len(tf_idf_matrix)):
			for j in range(i, len(tf_idf_matrix)): 
				self.cluster_matrix[i][j] = distance.euclidean(tf_idf_matrix[i], tf_idf_matrix[j])
				self.cluster_matrix[j][i] = self.cluster_matrix[i][j]

		for i in range(len(self.cluster_matrix)):
			self.cluster_matrix[i][i] = np.inf
		print "Distance matrix Done -----------"

		while len(self.cluster_matrix) > 7:
			(i, j) = np.unravel_index(self.cluster_matrix.argmin(), self.cluster_matrix.shape)

			for k in range(len(self.cluster_matrix)):
				if i == k:
					continue
				if j == k:
					continue

				num = float(len(self.cluster[i]) + len(self.cluster[j]) + len(self.cluster[k]))
				alpha_i = float(len(self.cluster[i]) + len(self.cluster[k])) / num
				alpha_j = float(len(self.cluster[j]) + len(self.cluster[k])) / num
				beta = -float(len(self.cluster[k]) / num)

				self.cluster_matrix[i][k] = float(alpha_i * self.cluster_matrix[k][i]) + float(alpha_j * self.cluster_matrix[k][j]) + float(beta * self.cluster_matrix[i][j])
				self.cluster_matrix[k][i] = self.cluster_matrix[i][k]

			self.cluster_matrix = np.delete(self.cluster_matrix, j, 0)
			self.cluster_matrix = np.delete(self.cluster_matrix, j, 1)

			self.cluster[i].extend(self.cluster[j])
			self.cluster = np.delete(self.cluster, j, 0)
			print len(self.cluster_matrix)

		print "Clustering DONE ----------------"
		print "Start Clasification --------------"

	def get_cluster(self):
		self.preprocess('comp.csv')
		self.tf_idf()
		self.clustering()

		match_dict = dict(zip(self.category, self.cluster))
		
		with open('persona.json') as f:
			persona_dict = json.load(f)

		persona_category = None
		for category, cluster in match_dict.iteritems():
			if self.name in cluster:
				persona_category = category
			print len(cluster)

		return persona_dict[persona_category]

	def get_prediction(self):
		self.preprocess('comp.csv')
		self.tf_idf()
		# self.clustering()

		match_dict = dict(zip(self.category, self.cluster))
		
		with open('persona.json') as f:
			persona_dict = json.load(f)

		with open('pers.json') as g:
			random_dict = json.load(g)

		random_keys = [key for key in random_dict.keys()]

		for i in range(2000):
			if random_dict[str(i)]["Gender"] != self.pers[0]:
				random_keys.remove(str(i))
				continue

			if random_dict[str(i)]["Age"] != self.pers[1]:
				random_keys.remove(str(i))
				continue

			if random_dict[str(i)]["Personal Interests"] != self.pers[2]:
				random_keys.remove(str(i))
				continue

			if random_dict[str(i)]["Attitude"] != self.pers[3]:
				random_keys.remove(str(i))
				continue

		persona_dict = {}
		secure_random = SystemRandom()
		p1 = random_keys[0]
		random_keys.remove(p1)
		p2 = random_keys[1]
		p3 = secure_random.choice(random_dict.keys())
		print p1, p2, p3

		persona_dict["Persona1"] = random_dict[p1]
		persona_dict["Persona1"]["Demographics"] = persona_dict["Persona1"]["Gender"] + " " + persona_dict["Persona1"]["Age"]
		del persona_dict["Persona1"]["Gender"]
		del persona_dict["Persona1"]["Age"]

		persona_dict["Persona2"] = random_dict[p2]
		persona_dict["Persona2"]["Demographics"] = persona_dict["Persona2"]["Gender"] + " " + persona_dict["Persona2"]["Age"]
		del persona_dict["Persona2"]["Gender"]
		del persona_dict["Persona2"]["Age"]
		
		persona_dict["Persona3"] = random_dict[p3]
		persona_dict["Persona3"]["Demographics"] = persona_dict["Persona3"]["Gender"] + " " + persona_dict["Persona3"]["Age"]
		del persona_dict["Persona3"]["Gender"]
		del persona_dict["Persona3"]["Age"]

		return persona_dict

def magic():
	data = pandas.read_csv("../audiences_and_brands.csv")
	print data.loc[data["answer"] == "Armani"]




def main():
	# persona = Cluster('Visa', 'We are a global payments technology company', ("Male", "16-24", "Technology", "Alturistic"))
	# persona.get_cluster()
	magic()

if __name__ == '__main__':
	main() 



import os
import sys
sys.path.insert(0,'../')
from flask import Flask, render_template, request, redirect, session
import json
from flask_cors import CORS
import datetime

app = Flask(__name__, static_folder='../static', template_folder='../templates')

def start_server():
    global app
    CORS(app)

print 'Starting to run the server'
